# 2. 2D and 3D Design

Assignment: 
- make a 2D and 3D design sketch for a possible final project
- create a page to document your progress of this week:
- how did you make your 2D design
- how did you make your 3D design
- add the download links of:
- 2D design file
- 3D design file

For this weeks assignment we had to design something in 2D and 3D. For me it makes sense to use this weeks assignment to think about my final project and start to design parts of it in 2D and 3D so I can reuse it later on. 


## 2D Design

In my 2D assignment I decided to create a basic airfoil like form which I later can use to 3D design a wingblade or cut it out to have a model of the profile of a turbine-blade. I used librecad as my program of choice as it is open source and free to use. It is also pretty easy and self explaining to use thats why this assignment was done pretty quickly and because a basic airfoil is quickly build.

First I created one single airfoil with a straight line and the spline function in librecad. 

![](../images/splinethroughpoints.png)

_Spline through points_

The 2D piece consists of a flat base and a in beginning thick rounded top and a pointy end.
I wanted multiple airfoil profiles, because a single blade usually gets smaller and sometimes twisted to the end. 
I will provide the software links down below for people who are interested. 

![](../images/Scalingoptionen.png)

_Scaling options in librecad_

For scaling librecad has a useful tool, which can also simultaneously can create multiple copies of your drawing which are scaled stepwise. To enable this feature check the "Multiple Copies" option on the right side of the window and type in the number of stepwise scaled copies you want. The result should look something like this. 

![](../images/Scaledmultiplication.png)

_Scaled multiple_

The outer airfoil is the one created myself. As you can see it is just a simple model.  The inner airfoils are created by librecad by the mentioned scaling opting. This model can be used to explain the different profile sizes of an airfoil over the course of it. 

## 3D Design 

For my 3D assignment I designed a laptop-stand to get used with fusion and its abilities. 
First I made a  rough outline on paper and brainstormed the parameters for the object. Normally this also can be done directly in Fusion but prefer pen and paper more for this purpose.



![](../images/drawing2d.jpg)
_rough drawing on paper_ 

First I measured the base of my laptop, to create a perfect fit on the stand. Then I tried to get a feeling of the perfect angle and height for me.
The dimensions of my base are 24,6 cm x 35,6 cm. The laptop stand should have an initial height of 2 cm and an angle of 20 degrees. While creating the basic forms for my laptop-stand I also scribbled a little bit to gather some ideas about the construction and design of my object. 

After I determined the dimension of my project I started to work on my design. Even though I created some sketches on paper regarding the design I was not satisfied with, so I just played around with object in Fusion. 

### Fusion 360 

Like I mentioned before I used _Fusion 360_ for this assignment, because I think it is a very powerful tool and it is easy to start with but hard to master kind of a program. 
After installing the program, starting it and opening a new project. I started to create the sides with basic shapes. For my purpose A square and a triangle were perfect. So after creating the basic structure I started shaping it. In the pictures I provide now the parameters are not the same I talked about before, because I already finished the object and just redo some parts of it in Fusion for documentation purposes. 




![](../images/basicstructure.png)

_Basic structure_

![](../images/trimm.png)

_Trim_

![](../images/filet.png)
_Fillet_  

![](../images/spline.png)

_Spline_

![](../images/mirror.png)

_Mirror_

![](../images/laptopstand2d.png)

_Final 2D Design of the laptop-stand_ 

In  the above pictures I showed and outline what basic tools I used to create my final 2D Design for the laptop-stand in _Fusion 360_. I just used the above depicted functions and played around with the form and structure till I was satisfied how it looks like.

From here on I extruded the surface of my 2D design to be able to work on my project in 3D. This can be done quick through the simple press of the "e" key on your keyboard in fusion. I want to indclude a jointed design thats why the thickness of my parts should be equal to the thickness of the material I want to us. In my case I thought about 3 mm MDF wood. After extruding I rotate my parts horizontally so I could assemble it and align some supports. I want a gravity lock type of support and for creating those, you could either do it directly in the sketch and include fitting holes or do it in 3D. If you want to create the holes for the supports in 2D, the tools I used above should be sufficient to create them. 

In 3D modeling it is a little bit different.

![](../images/align3D.png)

_Support structure_ 

After creating a fitting support beam and deciding on the type of joint, the beam should be aligned to the place where it will be installed later 

![](../images/gothroughwohole.png)

_Right alignment_

The beam be moved with the "m" key. 

![](../images/combinetool.png)

_Combine tool_

The easiest way to create the hole, is to use the combine tool which can be found in the by default in the top positioned toolbar. 

![](../images/combineexecution.png)

_Combining_

After selecting the Combine tool select the object where the hole will be placed on as the "Target Body" and the object which has to be fitted there as the "Tool Bodies". Select the Keep Tools checkbox.
If everything done right the target body should be highlighted blue and the tool body red. Just press ok to execute. 


![](../images/showingthejoint.png)

_Joint female_

The result should look like the picture above. Now there is a perfect fit for the support beam. To be completely sure if the joint will you can check it again with the measure tool. 




![](../images/laptopstand3d.png)

_Final 3D Design_

The picture shows the final result of my laptop-stand.

A new or better approach for a similar design next time would be a parametric design. Parametric design means that the the length of the object is not defined by a constant but as a variable or with equations which denote the relationship the variables have together. The plus side is that the laptop-stand can changed quickly for every kind of laptops, regarding size. 
The downside here is that the parametric approach to design, is that it is connected to a lot of trouble shooting especially if you are not used to parametric design. 

