# 1. Project management

This week I worked on defining my final project idea and started to getting used to the documentation process, while also creating a website to publish my projects.

I have little to no experience in creating a website so this assignment consists a lot of trial and error and learning. 
So to get started I downloaded brackets, because it is free to use and offers a very wide pallet of extensions. I use markdown as my language of choice because I am not very familiar with html and css, I can not guarantee finishing this assignment when I don't know anything about the "tools" I use to create it. Markdown got recommended by my lecturer as a beginner friendly easy to use language for web development. So markdown it is. Before getting started I installed a live preview plug in in Brackets to see my process and get some immediate feedback how things are going on the front end side of things. My lecturer thankfully also provided us Template which we just have to fill with our project documentations and customize it, if we feel like it. 


![](../images/week02/BracketsLivePreview.png)
_Picture 1_


In the picture I provided above, you can see the current view I have as I work on this Website. At the top I can fill my Type and in the Bottom half of the Website I see the current status my Website has.
After also filling up the About me page a little bit my next concern is how to publish my documentation site through git. The git [page](https://pages.github.com/) was very helpful for my purposes and I just followed the depicted instructions step by step. 

Before I could continue pushing and publishing my documentation website I had to merge my git repository with my local folder. Because I made changes on both repositories without synchronizing. 

![](../images/week02/Git add.png)
_Picture 2_

First I checked the status. The used code can be seen in the picture above but I will also provide all the used commands at the end of this paragraph. In order to merge I had to pull the repository through git bash to sync my local machine with the gitlab repository and then push the synced folder to upload the previous made changes of my local to gitlab. After this was done my website was accessible and the lecturer now also can call up my documentation. 

After I made sure the website was accessible I continued working on the content. I work on my local machine and  after I am finished for the session I just push it again. 

![](../images/week02/Git adding picture.png)

_Picture 3_

I used the commands above in the git bash shell. It is nearly the same commands without the previous pull command. I don't have to use it because from now on I will try to do everything from my local machine and push it after I am finished. So this step will become obsolote and I only have to push after changing and editing files on my gitlab. As I make further changes in the future to my website, I will continue documenting on this page.

## Useful links

- [Brackets](http://brackets.io/)
- [CSS](https://css-tricks.com/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)

## Code Example

Git commands I used.

```
pwd <- showing the current directory
cd <- changing directory
cd .. <- going one folder back
git status <- showing the current status of your git repository
git pull <- pulling the files from your git repository
git add . <- adding all files from your directory to your commit
git commit -m <- saving the files into your cache and adding a comment
git push <- updating the files and adding to your online repository

```