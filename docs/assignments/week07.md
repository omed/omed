# 6. Electronics Design

Assignment

modify the Fundamentals Hello board or any other existing board, adding at least:
an led connected to the microcontroller
a button connected to the microcontroller
make your modified board:
CNC mill the PCB
solder the components
program the board with a blink
create a page to document your progress of this week:
how did you design your board
video of your board blinking
add a download link of:
your board design files
