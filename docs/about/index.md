# About me

Hello! My name is Omed Abed. I study Mobility and Logistics B.Sc and come from an small town in West Germany.

## My background

My family comes from Afghanistan, but I was born in south germany. When I was 6 years old we moved to west germany and stayed here. I am very interested in technology and everything that has something to do with technology. I used to repair mobile phones and computers for fun and often resell them after I fixed them. I also like hands on work where I can use my hands, especially woodworking. I would like to learn more about electronics and PCB making. My focus is to create something where the focus lies on electricity and its conversion. For example the conversion from chemical or kinetic energy to electrical.

## Previous work

- Construction and Development of the FabHouse, as my interdisciplinary project. 
- Working experience in the HSRW FabLab Kamp-Lintfort.

